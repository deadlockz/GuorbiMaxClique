#!/bin/bash
MAINJAVA=Maxclique
CLASSDIR="-classpath /opt/gurobi751/linux64/lib/gurobi.jar:."
JFLAG="-d . ${CLASSDIR}"

javac $JFLAG $MAINJAVA.java
java $CLASSDIR $MAINJAVA $1

#?? jar cfe aout.jar $MAINJAVA $MAINJAVA.class
#?? java -jar $CLASSDIR aout.jar
