import gurobi.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Maxclique {
  private static double _solution;
  
  public static void main(String[] args) {
    if (args.length != 1) {
        System.out.println("datei vergessen");
        return;
    }
    HashSet<String> linksA = new HashSet<String>();
    HashSet<String> linksB = new HashSet<String>();
    HashSet<String> proteins = new HashSet<String>();
    
    try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
        String sCurrentLine;

        while ((sCurrentLine = br.readLine()) != null) {
            String[] parts = sCurrentLine.split("\t");
            linksA.add(parts[0] + "\t" + parts[1]);
            linksB.add(parts[1] + "\t" + parts[0]);
            if (!proteins.contains(parts[0])) proteins.add(parts[0]);
            if (!proteins.contains(parts[1])) proteins.add(parts[1]);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
    
    int lcount = linksA.size();
    System.out.printf("existing links:    %d\n", lcount);
    System.out.printf("existing proteins: %d\n", proteins.size());
    
    System.out.print("\n");
    System.out.printf("Check for a->b  and b->a in file '%s'\n", args[0]);
    
    for (String link : linksA) {
        if (linksB.contains(link)) {
            System.out.printf("%s  bo!\n", link);
        }
    }
    
    HashSet<String> missing = new HashSet<String>();
    for (String p1 : proteins) {
        for (String p2 : proteins) {
            String check1 = p1 + "\t" + p2;
            String check2 = p2 + "\t" + p1;
            if (
                !p2.equals(p1) &&
                !linksA.contains(check1) &&
                !linksA.contains(check2) &&
                !missing.contains(check1) &&
                !missing.contains(check2)
            ) {
                missing.add(check1);
            }
        }
    }

    System.out.print("\n");
    System.out.printf("missing links: %d\n", missing.size());
    
    for (String l : missing) {
        System.out.printf("  %s\n", l);
    }

    /*
    ./Makejava.sh HPRD_Heinz_reduced.txt 
    existing links:    3540
    existing proteins: 798

    Check for a->b  and b->a in file 'HPRD_Heinz_reduced.txt'

    missing links: 314463
    * 
    * 798 über 2:  318003
    */
    
    try {
        String name = "Max_Clique";
        GRBEnv env = new GRBEnv();
        env.set(GRB.IntParam.OutputFlag, 0);
        GRBModel model = new GRBModel(env);
        GRBVar[] xp    = new GRBVar[proteins.size()];

        model.set(GRB.StringAttr.ModelName, name);
        GRBLinExpr expr = new GRBLinExpr();
        int i = 0;
        for (String p : proteins) {
            xp[i] = model.addVar(0.0, 1.0, 1.0, GRB.BINARY, p);
            expr.addTerm(1.0, xp[i]);
            i++;
        }
        model.setObjective(expr, GRB.MAXIMIZE);
        // only works with that line, else:
        // Error code: 10005. No variable names a vailable to index
        model.write(name + ".lp");

        for (String link : missing) {
            String[] pr = link.split("\t");
            
            GRBLinExpr newConstraint = new GRBLinExpr();
            newConstraint.addTerm( 1.0, model.getVarByName(pr[0]));
            newConstraint.addTerm(+1.0, model.getVarByName(pr[1]));
            model.addConstr(newConstraint, GRB.LESS_EQUAL, 1, pr[0] + "_TO_" + pr[1]);
        }

        model.update();
        model.optimize();

        _solution = model.get(GRB.DoubleAttr.ObjVal);
        System.out.print("Lösung: ");
        System.out.println(_solution);

        for (GRBVar v : model.getVars()) {
            double xsol = v.get(GRB.DoubleAttr.X);
            if (xsol > 0 || xsol < 0) {
                System.out.print(v.get(GRB.StringAttr.VarName));
                System.out.print(" = ");
                System.out.println(xsol);
            }
        }

        model.write(name + ".lp");
        model.dispose();
        env.dispose();

    } catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
          e.getMessage());
    }

  }
}
